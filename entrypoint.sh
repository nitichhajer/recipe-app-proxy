#!/bin/sh

set -e

# substitute env variables in the conf file
envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf

# to allow nginx to run in the foreground of the docker application
nginx -g 'daemon off;'

